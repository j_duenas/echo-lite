<!DOCTYPE HTML>
<html>
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
    <meta charset="UTF-8">
    <!--[if gte IE 9]>
    <style type="text/css">
        .gradient {
           filter: none;
        }
    </style>
    <![endif]-->
    <title><?php wp_title('&mdash;');?></title>
    <?php wp_head();?>

</head>
<body>
    <?php wp_footer();?>
</body>
</html>
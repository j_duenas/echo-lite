/*Configuration*/
    var p = require('./package.json');
    var config = {
        ftp: {
            user: 'username',
            password: 'password',
            host: 'server',
            appSource: 'dist/**/*',
            appDest: '/public_html',
        },
        dirs: {
            watchCompass: 'app/theme/scss/**/*.scss',
            watchApp: 'app/**/*',
            pushApp: 'app/**/*',
            distApp: 'dist/',
            compassSCSS: 'app/theme/scss/style.scss',
            compassConfig: './config.rb',
            compassCSS: 'app',
            compassSASS: 'app/scss',
            compassTMP: './tmp'
        }
    };

/*Required Packages*/
    var fs = require('fs');
    var gulp = require('gulp');
    var compass = require('gulp-compass');
    var ftp = require('gulp-ftp');
    var util = require('gulp-util');

/*Tasks*/

    //Watch
    gulp.task('watch', function(){
        gulp.watch(config.dirs.watchCompass, ['compass']);
        gulp.watch(config.dirs.watchApp, ['pushTheme']);
    });

    //Push App
    gulp.task('pushTheme', function(){
        return gulp.src(config.dirs.pushApp)
            .pipe(gulp.dest(config.dirs.distApp));
    });

    //Compass Compile
    gulp.task('compass', function(){
        gulp.src(config.dirs.compassSCSS)
            .pipe(compass({
                config_file: config.dirs.compassConfig,
                css: config.dirs.compassCSS,
                sass: config.dirs.compassSASS
            }))
            .pipe(gulp.dest(config.dirs.compassTMP));
    });

    //FTP
    gulp.task('ftp', function(){
        return gulp.src(config.ftp.appSource)
            .pipe(ftp({
                host: config.ftp.host,
                user: config.ftp.user,
                pass: config.ftp.password,
                remotePath: config.ftp.appDest
            }));
    });
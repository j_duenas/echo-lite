# Echo
Echo is a base template for Icon Graphics projects. It utilizes the following:
* gulp
* SASS/Compass
* Vagrant
* npm

### Gulp Tasks
##### watch
* Watches `app/theme/scss` for changes and triggers the `compass` task on detection.
* Watches `app` directory for any changes and triggers the `pushTheme` and `pushPlugins` task
##### pushTheme
* Copies files from `app/theme/theme_name` to `dist/wp-content/themes/theme_name`
##### pushPlugins
* Copies files from `app/plugins` to `dist/wp-content/plugins`
##### compass
* Compiles SASS using compass, using `./config.rb`, outputs style.css to `app/theme`
##### themeHeader
* Grabs information from package.json file and populates `app/theme/scss/vendor/wp/_wpthemeheader.scss` with WordPress theme comments (Theme Name, Author URI, etc.)
##### ftp
* FTPs theme and plugin files from `dist` to remote server (configured via `config` var in `gulpfile.js`

### To Do
* Make `watch`'s tasks less redundant